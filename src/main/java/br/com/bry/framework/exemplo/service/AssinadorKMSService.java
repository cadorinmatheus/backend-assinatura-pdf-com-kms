package br.com.bry.framework.exemplo.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import br.com.bry.framework.exemplo.configuration.ServiceConfiguration;
import br.com.bry.framework.exemplo.util.UUIDChave;

@Service
public class AssinadorKMSService {
	@Autowired
	private RestTemplate restTemplate;

	private static String obterTokenKMS(String cpf, String tokenCloud, int quantidadeAssinaturas, int tempo, String kmsCredencial)
			throws JSONException {

		RestTemplate restTemplate = new RestTemplate();

		// Necessário primeiro obter o uuid do certificado no compatimento
		// O retorno é uma lista de UUIDChave, e deve ser escolhido um
		// A lista só retorna mais de um elemento se o usuário conter mais de um
		// certificado no KMS
		HttpHeaders uuidHeader = new HttpHeaders();
		uuidHeader.setContentType(MediaType.APPLICATION_JSON);
		uuidHeader.add("Authorization", tokenCloud);

		HttpEntity<Object> requestEntity = new HttpEntity<>(null, uuidHeader);

		ResponseEntity<List<UUIDChave>> uuidResponse = null;
		try {
			uuidResponse = restTemplate
					.exchange("https://kms.bry.com.br/kms/rest/v1" + "/chaves/?vinculados=true&cpf=" + cpf, HttpMethod.GET,
							requestEntity, new ParameterizedTypeReference<List<UUIDChave>>() {
							});
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
		}

		// Depois de obter os uuids, é necessário autorizar seu uso
		// É possível configurar por quanto tempo será autorizado e para quantas
		// assinaturas será autorizado
		JSONObject requestParams = new JSONObject();
		requestParams.put("operacao", "ASSINATURA");
		requestParams.put("quantidade", quantidadeAssinaturas);
		requestParams.put("expiracao", tempo);
		requestParams.put("aplicacao", "BRy KMS Postman");
		requestParams.put("descricao", "Teste de Assinatura PIN WS v1");

		HttpHeaders authorizationHeader = new HttpHeaders();
		authorizationHeader.setContentType(MediaType.APPLICATION_JSON);
		authorizationHeader.add("kms_credencial", kmsCredencial);
		authorizationHeader.add("kms_credencial_tipo", "PIN");
		authorizationHeader.add("Authorization", tokenCloud);

		HttpEntity<Object> requestAuthorization = new HttpEntity<>(requestParams.toString(), authorizationHeader);
		ResponseEntity<String> authorizationResponse = null;
		try {
			authorizationResponse = restTemplate.exchange(
					"https://kms.bry.com.br/kms/rest/v1" + "/chaves/" + uuidResponse.getBody().get(0).getUuid() + "/autorizacoes",
					HttpMethod.POST, requestAuthorization, String.class);
		} catch (HttpClientErrorException e) {
			e.printStackTrace();
		}
		String authorizationResponseString = authorizationResponse.getBody().toString();

		JSONObject authorizationResponseJson = new JSONObject(authorizationResponseString);
		return (String) authorizationResponseJson.get("token");

	}

	public String assinar(HttpServletRequest request) throws JSONException {

		ResponseEntity<String> responseInitialize = null;

		//Cria requisição para enviar ao servidor do HUB
		System.out.println("Parseando requisicao recebida do front");
		HttpEntity<?> requestToAPI = this.getHttpEntity(request);

		//Envia requisição ao HUB
		System.out.println("Enviando requisição ao HUB2");
		responseInitialize = restTemplate
				.exchange("https://hub2.bry.com.br/fw/v1/pdf/kms/lote/assinaturas", HttpMethod.POST, requestToAPI, String.class);

		//Obtem url de download do documento assinado para retornar ao front
		System.out.println("Obtendo link de download do documento assinado para retornar ao frontend");
		JSONObject body = new JSONObject(responseInitialize.getBody());
		return body.getJSONArray("documentos").getJSONObject(0).getJSONArray("links").getJSONObject(0).getString("href");

	}

	private HttpEntity<?> getHttpEntity(HttpServletRequest request) throws JSONException {

		List<MultipartFile> currentDocumentStreamContentValue = ((StandardMultipartHttpServletRequest) request).getMultiFileMap()
				.get("documento");

		Resource resourceOriginalDocument = null;

		//Obtem o documento PDF
		if (currentDocumentStreamContentValue != null && !currentDocumentStreamContentValue.isEmpty()) {
			resourceOriginalDocument = currentDocumentStreamContentValue.get(0).getResource();
		}

		Resource resourceImage = null;

		List<MultipartFile> currentImageStreamContentValue = ((StandardMultipartHttpServletRequest) request).getMultiFileMap()
				.get("imagem");
		//Obtem imagem caso foi enviada
		if (currentImageStreamContentValue != null && !currentImageStreamContentValue.isEmpty()) {
			resourceImage = currentImageStreamContentValue.get(0).getResource();
		}

		JSONObject dadosAssinatura = new JSONObject();
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

		//Configura JSON dados_assinatura
		dadosAssinatura.put("perfil", request.getParameterValues("perfil")[0]);
		dadosAssinatura.put("algoritmoHash", request.getParameterValues("algoritmoHash")[0]);
		String[] cpf = request.getParameterValues("signatario");
		dadosAssinatura.put("signatario", cpf[0]);
		map.add("dados_assinatura", dadosAssinatura.toString());
		//Adiciona o documento na requisicao
		map.add("documento", resourceOriginalDocument);

		//Configura header da requisição
		final HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", ServiceConfiguration.AUTHORIZATION); // Token de acesso (access_token), obtido no BRyCloud 
		//(https://api-assinatura.bry.com.br/api-assinatura-digital#autenticacao-apis). Para uso corporativo, o
		//token deve ser da pessoa juridica e para uso pessoal, da pessoa fisica.
		
		//Define se a credencial sera um PIN ou TOKEN
		//Se for TOKEN, obtem um conforme parametros abaixo
		if (ServiceConfiguration.KMS_CREDENCIAL_TIPO == "TOKEN") {
			// PARÂMETROS ENVIADOS: CPF DO ASSINANTE, TOKEN DE AUTENTICAÇÃO NO BRY CLOUD,
			// QUANTIDADE MÁXIMA DE ASSINATURAS QUE O TOKEN IRÁ REALIZAR, TEMPO DE DURAÇÃO
			// DO TOKEN E PIN DO USUARIO NO BRY KMS
			String tokenKMS = obterTokenKMS(cpf[0], ServiceConfiguration.AUTHORIZATION, 600, 10000, ServiceConfiguration.KMS_CREDENCIAL);
			headers.add("kms_credencial", tokenKMS);
		} else {
			headers.add("kms_credencial", ServiceConfiguration.KMS_CREDENCIAL);

		}
		headers.set("kms_credencial_tipo", ServiceConfiguration.KMS_CREDENCIAL_TIPO);

		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		//Configura assinatura visivel
		if (request.getParameterValues("assinaturaVisivel")[0].equals("true")) {
			JSONObject imageConfiguration = new JSONObject();
			imageConfiguration.put("altura", request.getParameterValues("altura")[0]);
			imageConfiguration.put("largura", request.getParameterValues("largura")[0]);
			imageConfiguration.put("posicao", request.getParameterValues("posicao")[0]);
			imageConfiguration.put("pagina", request.getParameterValues("pagina")[0]);
			map.add("configuracao_imagem", imageConfiguration.toString());

			JSONObject textConfiguration = new JSONObject();
			if (request.getParameterValues("incluirTXT")[0].equals("true")) {
				textConfiguration.put("texto", request.getParameterValues("texto")[0]);
			}
			textConfiguration.put("incluirCPF", request.getParameterValues("incluirCPF")[0]);
			textConfiguration.put("incluirCN", request.getParameterValues("incluirCN")[0]);
			textConfiguration.put("incluirEmail", request.getParameterValues("incluirEmail")[0]);
			map.add("configuracao_texto", textConfiguration.toString());

		}
		//Adiciona imagem na requisicao caso for enviada
		if (resourceImage != null) {
			map.add("imagem", resourceImage);
		}
		System.out.println("Requisição para o HUB2 criada");

		return new HttpEntity<>(map, headers);
	}

}
